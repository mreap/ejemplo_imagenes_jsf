package util.archivos;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.model.file.UploadedFile;

import model.archivos.FileDTO;
import model.archivos.ManagerArchivos;
import util.JSFUtil;

@Named
@SessionScoped
public class BeanArchivo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private UploadedFile file;
	private String path;
	private List<String> listaImagenes;
	
	@EJB
	private ManagerArchivos mArchivos;
	
	@PostConstruct
	public void inicializar() {
		path=ManagerArchivos.RUTA_ARCHIVOS;
	}

	public BeanArchivo() {
		
	}
	
	public void upload() {
		String rutaCompleta="";
		
		if (file != null) {
			try {
				FileDTO fileDTO=new FileDTO(file.getFileName(), path, file.getSize(), file.getContent(), file.getContentType(), file.getInputStream());
				rutaCompleta=mArchivos.crearArchivo(fileDTO);
				JSFUtil.crearMensajeINFO("Archivo creado: "+rutaCompleta);
			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	public void actionListenerFindImagenes() {
		listaImagenes=mArchivos.findAllImagenes();
		JSFUtil.crearMensajeINFO("Archivos encontrados: "+listaImagenes.size());
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public List<String> getListaImagenes() {
		return listaImagenes;
	}

	public void setListaImagenes(List<String> listaImagenes) {
		this.listaImagenes = listaImagenes;
	}

}
