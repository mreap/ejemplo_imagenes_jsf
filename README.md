# ejemplo_imagenes_jsf

## Demo para el uso de imágenes con el framework JSF

Esta demostración implementa el uso de imágenes en una aplicación web JSF. Dichas imágenes están almacenadas fuera del context root de la aplicación web.
