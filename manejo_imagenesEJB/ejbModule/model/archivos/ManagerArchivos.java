package model.archivos;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import model.util.ModelUtil;

/**
 * Session Bean implementation class ManagerArchivos
 * @author mrea@utn.edu.ec
 */
@Stateless
@LocalBean
public class ManagerArchivos {
	
	public static String RUTA_ARCHIVOS="/home/curso/archivos/";
    /**
     * Default constructor. 
     */
    public ManagerArchivos() {
        
    }
    
    public String crearArchivo(FileDTO file) throws Exception {
		Date fecha=new Date();
		String rutaCompleta=file.getPath()+ModelUtil.date2StringYMD_(fecha)+"_"+file.getFileName();
		
		RandomAccessFile stream = new RandomAccessFile(rutaCompleta, "rw");
	    FileChannel channel = stream.getChannel();
	    
	    byte[] strBytes = file.getContent();
	    ByteBuffer buffer = ByteBuffer.allocate(strBytes.length);
	    buffer.put(strBytes);
	    buffer.flip();
	    channel.write(buffer);
	    stream.close();
	    channel.close();
		
		return rutaCompleta;
	}
    /**
     * Lee una carpeta y obtiene los nombres de todos
     * los archivos de imagenes jpg.
     * @return Lista de nombres de archivos de imagenes.
     */
    public List<String> findAllImagenes() {
    	Path carpeta=Paths.get(RUTA_ARCHIVOS);
    	List<String> nombresArchivos = new ArrayList<>();
    	try {
			Stream<Path> rutas=Files.list(carpeta);
			rutas.filter(Files::isRegularFile)
			.forEach(ruta->nombresArchivos.add(ruta.toString()));
			rutas.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return nombresArchivos;
    }

}
