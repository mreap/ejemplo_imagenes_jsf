package model.archivos;

import java.io.InputStream;
/**
 * Clase FileDTO para manejo de archivos.
 * @author mrea@utn.edu.ec
 *
 */
public class FileDTO {
	private String fileName;
	private String path;
	private String fullPath;
	private long size;
	private byte[] content;
	private String contentType;
	private InputStream inputStream;
	
	public FileDTO(String fileName, String path, long size, byte[] content, String contentType,
			InputStream inputStream) {
		super();
		this.fileName = fileName;
		this.path = path;
		this.fullPath = path + fileName;
		this.size = size;
		this.content = content;
		this.contentType = contentType;
		this.inputStream = inputStream;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFullPath() {
		return fullPath;
	}
	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
}

