package model.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Clase utilitaria para la capa modelo.
 * @author mrea@utn.edu.ec
 *
 */
public class ModelUtil {

	/**
	 * Verifica si una cadena es igual a null o tiene longitud igual a cero.
	 * @param cadena Cadena que va a verificarse.
	 * @return
	 */
	public static boolean isEmpty(String cadena) {
		if(cadena==null || cadena.length()==0)
			return true;
		return false;
	}
	/**
	 * Devuelve el valor del año actual.
	 * @return valor entero del año actual.
	 */
	public static int getAnioActual() {
		Date fechaActual=new Date();
		SimpleDateFormat formato=new SimpleDateFormat("yyyy");
		int anioActual=Integer.parseInt(formato.format(fechaActual));
		return anioActual;
	}
	
	/**
	 * Devuelve el valor del mes actual.
	 * @return valor correspondiente al mes actual.
	 */
	public static int getMesActual(){
		Date fechaActual=new Date();
		SimpleDateFormat formato=new SimpleDateFormat("MM");
		int mesActual = Integer.parseInt(formato.format(fechaActual));
		return mesActual;
	}
	
	/**
	 * Adiciona o resta dias a una fecha de tipo Date.
	 * @param date la fecha original.
	 * @param days el numero de dias a sumar o restar.
	 * @return la fecha resultante.
	 */
	public static Date addDays(Date date, int days) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
				
		return cal.getTime();
	}
	
	/**
	 * Diferencias entre dos fechas
	 * @param fechaInicial La fecha de inicio
	 * @param fechaFinal La fecha de fin
	 * @return el numero de dias entre dos fechas
	 */
    public static int diferenciasDeFechas(Date fechaInicial, Date fechaFinal) {

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fechaInicioString = df.format(fechaInicial);
        try {
            fechaInicial = df.parse(fechaInicioString);
        } catch (Exception ex) {
        }

        String fechaFinalString = df.format(fechaFinal); 
        try {
            fechaFinal = df.parse(fechaFinalString);
        } catch (Exception ex) {
        }

        long fechaInicialMs = fechaInicial.getTime();
        long fechaFinalMs = fechaFinal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        return ((int) dias);
    }
	/**
	 * Retorna la fecha en formato dd/MM/yyyy hh:mm
	 * @param fecha Fecha a transformar
	 * @return fecha formateada
	 */
	public static String date2String (Date fecha){
		SimpleDateFormat formato=new SimpleDateFormat("dd/MM/yyyy hh:mm");
		return formato.format(fecha);
	}
	
	/**
	 * Retorna la fecha en formato yyyy/MM/dd hh:mm:ss
	 * @param fecha Fecha a transformar
	 * @return fecha formateada
	 */
	public static String date2StringYMD (Date fecha){
		SimpleDateFormat formato=new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		return formato.format(fecha);
	}
	
	/**
	 * Retorna la fecha en formato yyyyMMdd_hhmmss
	 * @param fecha Fecha a transformar
	 * @return fecha formateada
	 */
	public static String date2StringYMD_(Date fecha){
		SimpleDateFormat formato=new SimpleDateFormat("yyyyMMdd_hhmmss");
		return formato.format(fecha);
	}
}
